'use strict';

// first part of homework#4

let initialArray = [0, 2, 4, 8, 9, 10, 22, 1991];
let modifiedArray = map(Math.exp, initialArray);

function map(fn, array) {
    let newArray = array.map(fn);
    return newArray
}

console.log(initialArray);
console.log(modifiedArray);


// second part of homework#4

let userAge = +prompt('How old are you?', 'please enter only number');

checkUserAge(userAge);
checkAgainUserAge(userAge);

function checkUserAge(age) {
    age > 18 ? alert('Welcome') :
        confirm('Is there permission from the parents?') ? alert('Ok. Let me see') :
        alert('You can not pass');
}

function checkAgainUserAge(age) {
    if (age > 18 || confirm('Is there permission from the parents?')) {
        alert('You may come in');
    } else {
        alert('You can not pass');
    }
}