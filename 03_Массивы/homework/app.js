'use strict';

let styles = ['Jazz', 'Blues'];
console.log(styles);

styles.push('Rock\'n\'roll');
console.log(styles);

changingAverageEl(styles);

function changingAverageEl(arr, averageEl) {
    for (let i = 0; i <= arr.length; i++) {
        averageEl = Math.floor(arr.length / 2);
        return arr[averageEl] = 'Classic';
    }
}
console.log(styles);

let firstEl = styles.shift();
console.log('first element of this array is ' + firstEl);
console.log(styles);

styles.unshift('Rap', 'Reggae');
console.log(styles);