'use strict';

let x = 6;
let y = 14;
let z = 4;
let firstEquation = x += y - x++ * z;

x = 6;
y = 14;
z = 4;
let secondEquation = z = --x - y * 5;

x = 6;
y = 14;
z = 4;
let thirdEquation = y /= x + 5 % z;

x = 6;
y = 14;
z = 4;
let fourthEquation = z - x++ + y * 5;

x = 6;
y = 14;
z = 4;
let fifthEquation = x = y - x++ * z;


document.write('x += y - x++ * z = ' + firstEquation + '</br>');
document.write('z = --x - y * 5 = ' + secondEquation + '</br>');
document.write('y /= x + 5 % z = ' + thirdEquation + '</br>');
document.write('z - x++ + y * 5 = ' + fourthEquation + '</br>');
document.write('x = y - x++ * z = ' + fifthEquation + '</br>');
document.write('Обьяснение: Все просто - здесь все дело в приорите операторов (есть таблица), в постфиксной и префиксной форме инкрементов и декрементов переменных' + '</br>');
document.write('P.S. Чтобы улучшить читаемость кода и минимизировать возможность ошибки лучше использовать скобки.');