'use strict';

const userName = prompt('What is yor name?', 'Please type only letters');
const userSurname = prompt('What is yor surname?', 'Please type only letters');
const userAge = prompt('How old are you?', 'Please type only letters');

let pageHeader = '<header style="height:200px; width:800px; border:3px solid red; text-align:center; line-height:200px;">header 800*200</header>';
let pageNav = '<nav style="height:400px; width:100px; border:3px solid orange; text-align:center; line-height:400px; float:left; ">nav 100*400</nav>';
let pageSection = '<section style="height:400px; width:800px; border:3px solid orange; text-align:center; line-height:400px;">section 700*400</section>';
let pageFooter = '<footer style="height:200px; width:800px; border:3px solid gray; text-align:center; line-height:200px;">footer 800*200</footer>';

document.write('Username is ' + userName, ', Last name of the User is ' + userSurname, ', User age is ' + userAge + '.');
document.write(pageHeader, pageNav, pageSection, pageFooter);


let userNumbers = [];
let arithmeticMean = 0;

function getUserNumber(number) {
    number = +prompt('Please enter a number', '0');
    if (!isNaN(number)) {
        userNumbers.push(number);
        getUserNumber();
    } else {
        alert('Invalid value');
    }
}

function getArithmeticMean(sum) {
    sum = 0;
    for (let i = 0; i < userNumbers.length; i++) {
        (sum += userNumbers[i]);
    }
    arithmeticMean = sum / userNumbers.length;
}

getUserNumber();
getArithmeticMean();

document.write('Arithmetic mean of the numbers is ' + arithmeticMean);