'use strict';

// first part of homework#6

function Player(name, club, age) {
    this.name = name;
    this.club = club;
    this.age = age;
}

const netherlandsTeam = [
    new Player('DB', 'Arsenal', 44),
    new Player('EDV', 'MU', 33),
    new Player('ED', 'Juventus', 25),
    new Player('RdB', 'Ajax', 30),
    new Player('FdB', 'PSV', 29),
    new Player('AN', 'Inter', 36),
    new Player('PC', 'Barcelona', 38),
    new Player('CC', 'Milan', 39),
]

function sortAscending(a, b) {
    if (a.age < b.age) {
        return -1;
    } else
    if (a.age > b.age) {
        return 1;
    } else
        return 0;
}

let sortAscendingTeam = netherlandsTeam.sort(sortAscending);
console.log(sortAscendingTeam);

// function sortDescending(a, b) {
//     if (a.age > b.age) {
//         return -1;
//     } else
//     if (a.age < b.age) {
//         return 1;
//     } else
//         return 0;
// }

// let sortDescendingTeam = netherlandsTeam.sort(sortDescending);
// console.log(sortDescendingTeam);