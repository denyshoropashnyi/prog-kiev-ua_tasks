'use strict';

const myDocument = {
    title: '',
    body: '',
    footer: '',
    date: '',
    creationApp: {
        title: function (val) {
            val = prompt('Please input your title', '');
            myDocument.title = val;
            document.write('<h3> title: ' + val + '</h3>');
        },
        body: function (val) {
            val = prompt('Please input your body', '');
            myDocument.body = val;
            document.write('<div> body: ' + val + '</div>')
        },
        footer: function (val) {
            val = prompt('Please input your footer', '');
            myDocument.footer = val;
            document.write('<footer> footer ' + val + '</footer>');
        },
        date: function (date) {
            date = prompt('Please input date', '');
            myDocument.date = date;
            document.write('<time> date: ' + date + '</time>')
        },
    },
    createContent: function (val) {
        val = confirm('Do you want add your content? ');
        if (val) {
            myDocument.creationApp.title();
            myDocument.creationApp.body();
            myDocument.creationApp.footer();
            myDocument.creationApp.date();
        } else {
            alert('You cancelled input');
        }
    }
}

myDocument.createContent();
console.log(myDocument);