'use strict';

class createNewUser {
    constructor() {
        this.firstName = prompt('What is your name?', '');
        this.lastName = prompt('What is your surname?', '');
        this.getLogin = () => {
            return this.firstName[0].toLocaleLowerCase() + this.lastName.toLocaleLowerCase()
        };
    }
}

let newUser = new createNewUser();

console.log(newUser);
console.log(newUser.getLogin());