'use strict';

let myTestArr = ['hello', 'world', 23, '23', null];

function filterBy(arr, type) {
    return arr.filter((name) => typeof(name) !== type);
}

let checkFunction = filterBy(myTestArr, 'string');
console.log(checkFunction);