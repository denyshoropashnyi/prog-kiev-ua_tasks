'use strict';

class createNewUser {
    constructor() {
        this.firstName = prompt('What is your name?', '');
        this.lastName = prompt('What is your surname?', '');
        this.birthday = prompt(`${this.firstName}, When is your date of birth?`, '').split('.');

        this.getLogin = () => {
            return this.firstName[0].toLocaleLowerCase() + this.lastName.toLocaleLowerCase()
        };
        this.getAge = () => {
            return new Date().getFullYear() - this.birthday[2];
        };
        this.getPassword = () => {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday[2];
        };
    }
}

let newUser = new createNewUser();

console.log(newUser);
console.log(newUser.birthday);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());