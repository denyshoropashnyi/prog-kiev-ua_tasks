'use strict';

const name = prompt('What is your name?', 'type here your name please');

const words = [
    'program',
    'monkey',
    'pretty',
    'cake',
];

const word = words[Math.floor(Math.random() * words.length)];

const answerArray = [];

for (let i = 0; i < words.length; i++) {
    answerArray[i] = '_';
}

answerArray.forEach(element => {
    
});

let remainLetters = word.length;


while (remainLetters > 0) {
    console.log(answerArray.join(' '));

    let guess = prompt('Try to guess one letter or press Cancel to exit game.');
    if (guess === null) {
        break;
    } else if (guess.length !== 1) {
        console.log('Please enter just one letter.');
    } else {
        for (let j = 0; j < word.length; j++) {
            if (word[j] === guess) {
                answerArray[j] = guess;
                remainLetters--;
            }
        }
    }
}

console.log(answerArray.join(' '));
console.log('Well done ' + name + '. The word is ' + word);