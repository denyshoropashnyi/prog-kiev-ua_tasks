'use strict';

// first part of classwork#2

let rectangleHeight;
let rectangleWidth;

getRectangleValues();
drawRectangle();

function getRectangleValues(userHeight, userWidth) {
    userHeight = +prompt('please enter rectangle height', '0');
    userWidth = +prompt('please enter rectangle width', '0');
    rectangleHeight = Math.round(userHeight);
    rectangleWidth = Math.round(userWidth);
}

function drawRectangle() {
    for (let i = 0; i < rectangleWidth; i++) {
        for (let j = 0; j < rectangleHeight; j++) {
            document.write('&#x2B50;');
        }
        document.write('<br/>');
    }
}


// second part of classwork#2

isAccessAllowed();

function isAccessAllowed() {
    let usernameEntered = prompt('Please enter your login', '');
    if (usernameEntered === 'Админ') {
        let userPassword = prompt('Enter admin password', '');
        if (userPassword === 'Черный Властелин') {
            alert('Добро пожаловать.')
        } else if (userPassword == null) {
            alert('Вход отменен.')
        } else {
            alert('Пароль неверен.')
        }
    } else {
        alert('Access denied.')
    }
}