'use strict';

// first part of homework#2

let a = +prompt('enter value a', '0');
let b = +prompt('enter value b', '0');
let result;

(a + b) < 4 ? result = 'it is not enough' : result = 'it is a lot';

console.log(result);


// second part of homework#2

let userLogin = prompt('Enter your login please', 'text');
let message;

userLogin == 'Вася' ? message = 'Привет' :
    userLogin == 'Директор' ? message = 'Здравствуйте' :
    userLogin == '' ? message = 'Нет логина' :
    message = '';

console.log(message);


// third part of homework#2

let startOfRange = +prompt('Enter start of range A please', '0');
let endOfRange = +prompt('Enter end of range B (B>A) please', '0');
let numbersSumInRange = 0;
let oddNumbersInRange = [];

for (let i = startOfRange; i <= endOfRange; i++) {
    numbersSumInRange += i;
    if (i % 2 !== 0) {
        oddNumbersInRange.push(i);
    }
}

console.log('Сумма чисел в указаном диапазоне = ' + numbersSumInRange + '; ' + ' Все нечетные числа в указанном диапазоне: ' + oddNumbersInRange);